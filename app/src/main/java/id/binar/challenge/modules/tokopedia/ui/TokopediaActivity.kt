package id.binar.challenge.modules.tokopedia.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.binar.challenge.databinding.ActivityTokopediaBinding


class TokopediaActivity : AppCompatActivity() {

    private var _binding: ActivityTokopediaBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityTokopediaBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}